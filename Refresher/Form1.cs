﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Net;
using Microsoft.Win32;
using System.Threading;
using System.Windows.Forms;

namespace Refresher
{
    public partial class Form1 : Form
    {
        string RegistryPath = "software\\PL-Refresher";
        string LabelStartText = "Последнее обновление -";
        string LabelStartHeaderText = "Авто обновлялка связи v 0.1";
        string URL = "http://ya.ru/";
        int DelayTimer = 400;
        int MaxPop = 3;
        int CurrentPop = 0;
        bool HideSettinsOnStart = true;
        bool AccessClose = false;
        string DataUpdate = "еще не обновлено";

        public Form1()
        {
            InitializeComponent();
        }

        // Загрузка настроек по умолчанию
        private bool LoadSettings()
        {
            string stringURL;
            int intDelay;

            RegistryKey readKey = Registry.LocalMachine.OpenSubKey(RegistryPath);
            try
            {
                stringURL = (string)readKey.GetValue("URL");
                intDelay = (int)readKey.GetValue("Delay");
            }
            catch (Exception ex)
            {
                SaveSettings(URL,DelayTimer);
                return false;
            }

            URL = stringURL;
            editSite.Text = URL;

            DelayTimer = intDelay;
            editDelay.Value = DelayTimer;

            readKey.Close();
            return true;
        }

        // Сохранение настроек
        private void SaveSettings(string url,int delay)
        {
            RegistryKey saveKey = Registry.LocalMachine.CreateSubKey(RegistryPath);
            saveKey.SetValue("URL", url);
            saveKey.SetValue("Delay", delay);
            saveKey.Close();
        }

        // Основной таймер
        private void Timer_Tick(object sender, EventArgs e)
        {
            OnTickMainTimer(); 
        }
        private void OnTickMainTimer()
        {
            TrayIcon.Icon = Icon.FromHandle(((Bitmap)TrayIconsList.Images[3]).GetHicon());
            string DataUpdateCurrent = null;
            CurrentPop++;
            DataUpdateCurrent = RequestOnUrl(URL);
            if (DataUpdateCurrent == null)
            {
                if (CurrentPop < MaxPop)
                {
                    DataUpdate = "проблемы с обновлением";
                    TrayIcon.Icon = Icon.FromHandle(((Bitmap)TrayIconsList.Images[1]).GetHicon());
                    SleepTimer.Enabled = true;
                    Timer.Stop();
                }
                else
                {
                    DataUpdate = "Невожможно обновить";
                    TrayIcon.Icon = Icon.FromHandle(((Bitmap)TrayIconsList.Images[2]).GetHicon());
                    Timer.Stop();
                    SleepTimer.Stop();
                }
            }
            else
            {
                TimeShowMenuItem.Text = DataUpdateCurrent;
                DataUpdate = DataUpdateCurrent;
                TrayIcon.Icon = Icon.FromHandle(((Bitmap)TrayIconsList.Images[0]).GetHicon());
                CurrentPop = 0;
                Timer.Enabled = true;
                SleepTimer.Enabled = false;
            }
        }

        // Интернет запрос к сайту
        private string RequestOnUrl(string url)
        {
            Application.DoEvents();
            string page=null;

            try
            {
                WebClient client = new WebClient();
                client.Encoding = System.Text.Encoding.UTF8;
                client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                page = client.DownloadString(url);
            }
            catch (Exception ex)
            {
                
            }

            if (page != null) return DateTime.Now.ToString();

            return null;
        }

        // Действия при загрузке программы
        private void Form1_Load(object sender, EventArgs e)
        {
            HideSettinsOnStart = LoadSettings();

            TrayIcon.Icon = Icon.FromHandle(((Bitmap)TrayIconsList.Images[3]).GetHicon());
            TrayIcon.Text = LabelStartHeaderText;

            Text = LabelStartHeaderText;

            Timer.Interval = DelayTimer * 1000;
            Timer.Enabled = true;

            //
            HelpHiht.SetToolTip(editSite, "Введите адресс сайта на котором будет проверятся наличие интернета");
            HelpHiht.SetToolTip(editDelay, "Введите интервал времени через который будет проверятся наличие интернета");
      
            
      
         }

        // Действие при первом показе формы
        private void Form1_Shown(object sender, EventArgs e)
        {
            if (HideSettinsOnStart) Visible = false;
            обновитьToolStripMenuItem_Click(sender,new EventArgs());
        }

        // Всплывающий текст
        private void ShowBallonText(string HeadText,string Text)
        {
            TrayIcon.BalloonTipTitle = HeadText;
            TrayIcon.BalloonTipText = Text;
            TrayIcon.ShowBalloonTip(1000);
        }

        // Кнопка с сохранением настроек 
        private void button1_Click(object sender, EventArgs e)
        {
            if (editSite.Text != "")
            {
                if ((editDelay.Value > 4) && (editDelay.Value < 1000))
                {
                    URL = editSite.Text;
                    DelayTimer = (int)editDelay.Value;
                    SaveSettings(URL, DelayTimer);
                    Timer.Interval = DelayTimer * 1000;
                    MessageBox.Show("Настройки сохранены");
                    Visible = false;
                }
                else MessageBox.Show("Некоректное время задержки");
            }
            else MessageBox.Show("Поле с сайтом должно быть заполнено");
        }

        // Меню - настройки
        private void настройкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Visible = true;
            Focus();
        }
        // Меню - выход
        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AccessClose = true;
            Close();
        }
        // Меню - обновить
        private void обновитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TrayIcon.Icon = Icon.FromHandle(((Bitmap)TrayIconsList.Images[3]).GetHicon());
            TimeShowMenuItem.Text = "Обновляю...";
            DataUpdate = RequestOnUrl(URL);
            if (DataUpdate == null)
            {
                DataUpdate = "Невожможно обновить";
                TrayIcon.Icon = Icon.FromHandle(((Bitmap)TrayIconsList.Images[2]).GetHicon());
            }
            else
            {
                TimeShowMenuItem.Text = DataUpdate;
                TrayIcon.Icon = Icon.FromHandle(((Bitmap)TrayIconsList.Images[0]).GetHicon());
            }
        }


        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!AccessClose) e.Cancel = true;
            Visible = false;
        }

        private void TrayIcon_DoubleClick(object sender, EventArgs e)
        {
            ShowBallonText(LabelStartHeaderText, LabelStartText + " " + DataUpdate);
        }

        private void SleepTimer_Tick(object sender, EventArgs e)
        {
            OnTickMainTimer();

        }
       
    }
}
